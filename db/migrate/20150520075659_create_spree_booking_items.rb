class CreateSpreeBookingItems < ActiveRecord::Migration
  def change
    create_table :spree_booking_items do |t|
      t.integer :spree_order_id
      t.string :spree_line_item_id
      t.date :date
      t.string :time

      t.timestamps null: false
    end
    add_index :spree_booking_items, :spree_order_id
    add_index :spree_booking_items, :spree_line_item_id
  end
end
