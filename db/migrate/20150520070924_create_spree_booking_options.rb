class CreateSpreeBookingOptions < ActiveRecord::Migration
  def change
    create_table :spree_booking_options do |t|
      t.boolean :bookable
      t.timestamps null: false
    end
  end
end
