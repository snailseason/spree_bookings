$ ->
  $input = $('.datepicker, .input-group.date').datepicker
    format: 'yyyy/mm/dd'
    startDate: '+3d'
    autoclose: true
    language: $('html').attr('lang')