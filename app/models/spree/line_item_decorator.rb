Spree::LineItem.class_eval do
  has_one :booking_item, class_name: 'Spree::BookingItem', foreign_key: 'spree_line_item_id'
end