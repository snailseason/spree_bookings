Spree::Order.class_eval do

  def no_bookings?
    false
  end

  def all_bookings?
    true
  end

  checkout_flow do
    go_to_state :address, if: ->(order) { order.no_bookings? }
    go_to_state :delivery, if: ->(order) { order.no_bookings? }
    go_to_state :booking
    go_to_state :payment, if: ->(order) { order.payment_required? }
    go_to_state :confirm, if: ->(order) { order.confirmation_required? }
    go_to_state :complete
    remove_transition from: :delivery, to: :confirm
  end

end