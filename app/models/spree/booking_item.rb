class Spree::BookingItem < ActiveRecord::Base
  belongs_to :line_item, class_name: 'Spree::LineItem'
end
