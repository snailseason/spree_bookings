FactoryGirl.define do
  factory :booking_option, :class => Spree::BookingOption do |f|
    f.variant { |p| p.association(:variant) }
    f.bookable true
  end
end